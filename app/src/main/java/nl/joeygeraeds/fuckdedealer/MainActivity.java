package nl.joeygeraeds.fuckdedealer;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int turn = 0;

    public ArrayList<String[]> mainDeck = new ArrayList<>();

    EditText cardChoice;
    TextView cardTextView;
    ImageView cardMiddle;
    TextView scorePoints;

    public String[] newCardDetails;
    public String drawnCard;
    public int drawnCardNumber;
    public int score;
    List<String> cardNumbering;
    List<Integer> amountOfCards = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainDeck = createDeck();
        newCardDetails = mainDeck.get(0);
        drawnCard = newCardDetails[0];
        drawnCardNumber = Integer.parseInt(newCardDetails[1]);
        mainDeck.remove(0);
        cardChoice = (EditText) findViewById(R.id.gekozenKaart);
        cardTextView = (TextView) findViewById(R.id.kaart);

        cardMiddle = (ImageView) findViewById(R.id.cardMiddle);
        score = 0;
        scorePoints = (TextView) findViewById(R.id.scorePoints);
        cardNumbering = Arrays.asList(getResources().getStringArray(R.array.numberingOfCards));


        for(int i = 0; i<13; i++){
            amountOfCards.add(0);
        }

    }


    public class emptyDeck extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.newGame)
                    .setPositiveButton(R.string.ja, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            mainDeck = createDeck();
                            newCardDetails = mainDeck.get(0);
                            drawnCard = newCardDetails[0];
                            mainDeck.remove(0);
                            clearCards();
                        }
                    })
                    .setNegativeButton(R.string.nee, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }


    public void getCard(View v) {
        Log.e("aantalkaarten", Integer.toString(mainDeck.size()));

        if(mainDeck.isEmpty()) {
            DialogFragment newDeck = new emptyDeck();
            newDeck.show(getSupportFragmentManager(), "newGame");
        }

        drawnCardNumber = Integer.parseInt(newCardDetails[1]);

        if(!cardChoice.getText().toString().equals("")){
            String choice = cardChoice.getText().toString();

            if(checkCard(choice, drawnCard)) {

                if(turn == 0){
                    score = score + 5;
                    scorePoints.setText(String.valueOf(score));

                } else {
                    score = score + 3;
                    scorePoints.setText(String.valueOf(score));
                }

                drawnCard = flipCard(drawnCard);
                cardTextView.setText(R.string.juist);


            } else {

                if(turn >= 1){
                    String higherLower = higherCard(choice, drawnCardNumber);
                    cardTextView.setText(higherLower);
                    int newScore = lowerScore(higherLower, choice, drawnCardNumber);
                    score = ( score - newScore );
                    scorePoints.setText(String.valueOf(score));
                    drawnCard = flipCard(drawnCard);


                }else{
                    cardMiddle.setImageResource(R.drawable.achterkant);
                    String higherLower = higherCard(choice, drawnCardNumber);
                    cardTextView.setText(higherLower);


                }

            }

            turn++;



        }else{
            Toast.makeText(MainActivity.this, "Vul eerst uw kaart in", Toast.LENGTH_SHORT).show();
        }
    }




    private String flipCard(String drawnCard) {
        int resID = getResources().getIdentifier(drawnCard , "drawable", getPackageName());
        cardMiddle.setImageResource(resID);
        //Return een nieuwe kaart om mee te werken!
        newCardDetails = mainDeck.get(0);
        String newCard = newCardDetails[0];
        replaceDrawnCard(drawnCard, drawnCardNumber);
        turn = -1;
        mainDeck.remove(0);

        return newCard;
    }




    public ArrayList<String[]> createDeck(){

        ArrayList<String[]> deck = new ArrayList<>();
        deck.add(new String[]{"r_twee", "2"});
        deck.add(new String[]{"r_drie", "3"});
        deck.add(new String[]{"r_vier", "4"});
        deck.add(new String[]{"r_vijf", "5"});
        deck.add(new String[]{"r_zes",  "6"});
        deck.add(new String[]{"r_zeven", "7"});
        deck.add(new String[]{"r_acht", "8"});
        deck.add(new String[]{"r_negen", "9"});
        deck.add(new String[]{"r_tien", "10"});
        deck.add(new String[]{"r_boer", "11"});
        deck.add(new String[]{"r_vrouw", "12"});
        deck.add(new String[]{"r_koning", "13"});
        deck.add(new String[]{"r_aas",  "14"});
        deck.add(new String[]{"s_twee", "2"});
        deck.add(new String[]{"s_drie", "3"});
        deck.add(new String[]{"s_vier", "4"});
        deck.add(new String[]{"s_vijf", "5"});
        deck.add(new String[]{"s_zes", "6"});
        deck.add(new String[]{"s_zeven", "7"});
        deck.add(new String[]{"s_acht", "8"});
        deck.add(new String[]{"s_negen", "9"});
        deck.add(new String[]{"s_tien", "10"});
        deck.add(new String[]{"s_boer", "11"});
        deck.add(new String[]{"s_vrouw", "12"});
        deck.add(new String[]{"s_koning", "13"});
        deck.add(new String[]{"s_aas",  "14"});
        deck.add(new String[]{"h_twee", "2"});
        deck.add(new String[]{"h_drie", "3"});
        deck.add(new String[]{"h_vier", "4"});
        deck.add(new String[]{"h_vijf", "5"});
        deck.add(new String[]{"h_zes", "6"});
        deck.add(new String[]{"h_zeven", "7"});
        deck.add(new String[]{"h_acht", "8"});
        deck.add(new String[]{"h_negen", "9"});
        deck.add(new String[]{"h_tien", "10"});
        deck.add(new String[]{"h_boer", "11"});
        deck.add(new String[]{"h_vrouw", "12"});
        deck.add(new String[]{"h_koning", "13"});
        deck.add(new String[]{"h_aas",  "14"});
        deck.add(new String[]{"k_twee", "2"});
        deck.add(new String[]{"k_drie", "3"});
        deck.add(new String[]{"k_vier", "4"});
        deck.add(new String[] {"k_vijf", "5"});
        deck.add(new String[] {"k_zes", "6"});
        deck.add(new String[]{"k_zeven", "7"});
        deck.add(new String[]{"k_acht", "8"});
        deck.add(new String[]{"k_negen", "9"});
        deck.add(new String[]{"k_tien", "10"});
        deck.add(new String[] {"k_boer", "11"});
        deck.add(new String[]{"k_vrouw", "12"});
        deck.add(new String[]{"k_koning", "13"});
        deck.add(new String[]{"k_aas",  "14"});

        long seed = System.nanoTime();
        Collections.shuffle(deck, new Random(seed));
        deck.add(new String[]{"none", "0"});

        return deck;
    }




    public boolean checkCard(String choice, String drawnCard) {
        boolean rightCard = false;

        String cardValue = drawnCard.substring(2);

        if (choice.equals(cardValue)) {
            rightCard = true;
        }

        return rightCard;
    }



    public String higherCard(String choice, int drawnCardNumber) {
        String higherCard = "Lager";

        int choosenCardNumber = cardNumbering.indexOf(choice) + 2;
        Log.e("highest Card:", "these cards "+ choosenCardNumber + " " + drawnCardNumber );

        if (choosenCardNumber < drawnCardNumber) {
            higherCard = "Hoger";
        } else if(drawnCardNumber == 0){
            higherCard = "lege stok";
        }

        return higherCard;
    }




    public int lowerScore(String higherLower, String choise, int drawnCardNumber){
        int newScore;
        int choosenCardNumber = cardNumbering.indexOf(choise) + 2;

        if(higherLower.equals("Hoger")){
            newScore = drawnCardNumber - choosenCardNumber;
        } else{
            newScore = choosenCardNumber - drawnCardNumber;
        }

        return newScore;
    }


    public void replaceDrawnCard(String drawnCard, int drawnCardNumber) {
        String cardValue = drawnCard.substring(2);
        int rightCardPlace = getResources().getIdentifier(cardValue, "id", getPackageName());
        ImageButton variableCardButton = (ImageButton) findViewById(rightCardPlace);

        int resID = getResources().getIdentifier(drawnCard , "drawable", getPackageName());
        variableCardButton.setImageResource(resID);

        //twee zit op positie 0 dus er moet -2 gedaan worden
        int newNumber = drawnCardNumber - 2;
        amountOfCards.set(newNumber, amountOfCards.get(newNumber) + 1);

        if(mainDeck.isEmpty()) {
            DialogFragment newFragment = new emptyDeck();
            newFragment.show(getSupportFragmentManager(), "deck");
        }


    }

    public void showAmount(View v){
        Object tag = v.getTag().toString();

        int locate = cardNumbering.indexOf(tag);
        int amount = amountOfCards.get(locate);
        String Value = "";
        switch (locate){
            case 0:
                Value = "tweeën";
                break;
            case 1:
                Value = "drieën";
                break;
            case 2:
                Value = "vieren";
                break;
            case 3:
                Value = "vijven";
                break;
            case 4:
                Value = "zessen";
                break;
            case 5:
                Value = "zevens";
                break;
            case 6:
                Value = "achten";
                break;
            case 7:
                Value = "negens";
                break;
            case 8:
                Value = "tienen";
                break;
            case 9:
                Value = "boeren";
                break;
            case 10:
                Value = "vrouwen";
                break;
            case 11:
                Value = "koningen";
                break;
            case 12:
                Value = "azen";
                break;
        }
        if(amount == 0){
            Toast.makeText(MainActivity.this, "Er is nog geen "+ tag +" gespeeld", Toast.LENGTH_SHORT).show();
        }else if(amount == 1){
            Toast.makeText(MainActivity.this, "Er is al " + amount + " "+ tag +" gespeeld", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(MainActivity.this, "Er zijn " + amount + " " + Value + " gespeeld", Toast.LENGTH_SHORT).show();
        }
    }

    public void clearCards() {
        for(int i =0; i<cardNumbering.size(); i++){
            int rightCardPlace = getResources().getIdentifier(cardNumbering.get(i), "id", getPackageName());
            ImageButton variableCardButton = (ImageButton) findViewById(rightCardPlace);
            variableCardButton.setImageResource(R.drawable.transparant);
        }

        score = 0;
        scorePoints.setText(String.valueOf(score));

        amountOfCards.clear();
        for(int i = 0; i<13; i++){
            amountOfCards.add(0);
        }




    }


}
